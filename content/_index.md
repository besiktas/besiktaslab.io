---
title:
---

# papers

- [Tokenized Action Space Decision Transformer](/research/paper-tokenized-action-space-decision-transformer/)
  - Accepted at AAAI 2024 (workshop)
- [Challenges of Self-Supervised Learning for Unified, Multi-Modal, Multi-Task Transformer Models](/research/ssl-for-multimodal-transformers/)
  - Accepted at CSCI 2022
- [Detecting Adversarial Attacks Through Neural Activations](/research/paper-detecting-adversarial-attacks/)
  - Accepted at [workshop](https://aisecure-workshop.github.io/aml-iclr2021/papers) for ICLR 2021

# other

## [neural niche](http://neuralniche.com)

novel ideas for generative neural networks including:

- [using generative neural nets in keras to create ‘on-the-fly’ dialogue](https://neuralniche.com/posts/tutorial-for-keras-generative-models/)
- [dockerfile for tensorflow 0.7 with python3 on gpu](https://neuralniche.com/posts/tensorflow-dockerfile-python3/)
- [basics of setting up a containerized telegram bot](https://neuralniche.com/posts/telegram-bot/)

<!-- other:

- [Using Kubernete Jobs for one off ingestion of CSV's](https://neuralniche.com/posts/csv-to-postgres-on-k8s-for-model/) -->

### [How to Denoise Images with Neural Networks](https://hub.packtpub.com/how-to-denoise-images-neural-networks/) (2016)

### [Simple Transfer Learning Tutorial](https://hub.packtpub.com/transfer-learning/) (2016)

### [iMessage dashboard visualization](http://gitlab.com/besiktas/iskrt) (2015)

- interactive visualization tool for iMessage built using dc.js and crossfilter

### [kaggle bike project](http://github.com/grahamannett/bike-kaggle) (2014)

- old exploration and ipynb of a kaggle competition
- [ipython notebook](http://nbviewer.ipython.org/github/grahamannett/bike-kaggle/blob/master/main.ipynb)

# previous classwork

### uw amath (2018-2019)

- [Data Analysis of GAN Images With Traditional Data Analysis Methods](files/amath582final.pdf)

  - Using Traditional Supervised Learning Methods and Spectral Analysis to analyze images generated with Deep Learning GAN models

- [IPYNB Overview of Methods and Topics for Scientific Computing Course](files/581finalcomputationalnotebook/581finalcomputationalnotebook)

### cu appm (2013)

- [Stochastic Modeling Of Pseudomonos Syringae Growth](files/appm_project3.pdf)
  - final project: recreation of mathematical model in matlab incorporating random numbers, stochastic simulations and population carrying capacity
  - [Old Link - Broken](http://mathbio.colorado.edu/index.php/MBW:Stochastic_modeling_of_Pseudomonos_syringae_growth)
- [Network Models: Firing Rate, Feedforward And Recurrent Models](files/appm_project2.pdf)
  - long before I had much knowledge or even knew what deep learning was, did a project based on mathematical modeling of neural systems.
  - [Old Link - Broken](http://mathbio.colorado.edu/index.php/MBW:Network_Models:_Firing_Rate%2C_Feedforward_and_Recurrent_Models)

### econ (2013)

- [mathematica notebook on heston model](files/HestonModel.pdf)
