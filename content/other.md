---
author: graham annett
---
# VSCode Info (pre 2018)

Visual Studio Code (VS Code) is my primary code editor, and while it's powerful and versatile, there are certain features and configurations that I frequently need to reference. This page serves as a personal knowledge base for these common scenarios.

## Running Files Without a Framework

When starting small projects or prototypes, I often work without frameworks and minimize required tooling. VSCode's task system can be useful enough in these cases, though it requires some initial configuration. While VSCode offers built-in task templates for some tools/languages, you'll need to create custom configurations for python/golang (although I am sure they will eventually be added to the default).

### Basic Task Configuration

As an example for a minimal streamlined project, the following `tasks.json` can be used instead of requiring an external task runner:

```JSON {linenos=table}
{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "tasks": [
        {
            "label": "run file",
            "type": "shell",
            "command": "${config:python.pythonPath} ${file}",
            "group": {
                "kind": "build",
                "isDefault": true
            }
        }
    ]
}
```

I often forget to use `${python.pythonPath}` and `isDefault`, so it would be nice if you could include defaults or extra options in your user/workspace settings.

---

<!-- # useless sites i started and had something on at one point

_note: none of these work anymore, some were free domains i previously _

- [birdflu.ml](http://birdflu.ml/)
- [practice.gq](http://practice.gq/) → [practice.fyi](http://practice.fyi/)
- [thespot.ml](http://thespot.ml/)

--- -->

<br>

# trivial python notes

_These are not relevant at all anymore_

## installing xgboost on python3

<!-- xgboost isn't installable through pip for python3 by default. i always forget this, and have no interest in using graphlab create properties (i do not even remember what this means?) and i try to keep it isolated to virtualenv's so i sometimes forget what the process is -->

the best way to install is create virtualenv and project folder:

```bash
git clone --recursive https://github.com/dmlc/xgboost
cd xgboost
bash build.sh
cd python-package
pip3 install -e .
```

#### using pandas to change object feature columns to numerical category columns

useful for multiple reasons (i.e. save visual space when viewing, lots of category data is indecipherable, MOSTLY for using sklearn features and not having to dummy variables which are computationally expensive)

[another reason why cat is better than object dtypes for pandas](http://matthewrocklin.com/blog/work/2015/03/16/Fast-Serialization/)

```python
import pandas as pd

df = pd.read_csv() # or whatever
for x in df.dtypes[df.dtypes == 'object'].index:
    df[x] = (df[x].astype('category')).cat.codes
```

---

<br>

# go guide

All of this was useless/outdated when (wrote prior to 2014, its 2017 now), so ~~deleted~~

## domain DNS/TXT

in domain advanced dns settings, only requires:

```bash
- A @ (gitlab IP)
- TXT @ gitlab-pages-verification-code=
```

---

<!-- # you need to build a sauna in your backyard

its time to build (a sauna in your ab)

[sauna](/page/sauna) -->
