---
title: "Paper: Challenges of Self-Supervised Learning for Unified, Multi-Modal, Multi-Task Transformer Models"
aliases:
  - /research/ssl-for-multimodal-transformers/
draft: false
type: redirect
target: /files/challenges_of_ssl_transformer_paper.pdf
section: research
---