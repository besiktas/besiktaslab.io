---
title: "Paper: Tokenized Action Space Decision Transformer"
aliases:
  - /research/dt-action-tokenization/
tags:
  - transformers
type: redirect
target: /files/dt-action-tokenization.pdf
section: research
---

