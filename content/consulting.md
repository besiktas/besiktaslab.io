---
title: "Consulting Services"
custom_css: ["consulting.css"]
---

# From Concept to Production

I would love to help your organization transform AI ambitions into reality.

---

# Services

## AI Engineering

- RAG systems and vector search solutions
- LLM fine-tuning and agent development
- Production-ready implementation

## AI Strategy

- Assessment of AI opportunities and implementation roadmapping
- Technology selection and build vs. buy decision support

## AI Training

- Learn AI workflows that multiply developer productivity 2-3x
- Customized workshops for technical and non-technical teams
- Knowledge transfer to ensure self-sufficiency

# Areas of Expertise

- Machine Learning and AI
- Data Analysis and Visualization
- Software Development and Architecture

---

# Reach Out

The best way to get started is to contact me at [graham.annett＠gmail.com](mailto:graham.annett@gmail.com) with an overview, timeframe and budget. I am also happy to have an exploratory conversation to see what solutions might best address your AI challenges.

---

<div style="text-align: center;">
  <a href="/resume">View Full Resume</a> ∙ <a href="https://www.linkedin.com/in/graham-annett/">LinkedIn</a>
</div>
<!-- To allow centering need to use div above rather than markdown `[View Full Resume](/resume) ∙ [LinkedIn](https://www.linkedin.com/in/graham-annett/)` -->

<!--
Possibly link to some projects on github/elsewhere
---

## Key Projects

- **Research Assistant AI**: System for in-depth topic investigation
- **Enterprise RAG**: SharePoint-integrated solution for HR documents
- **Vector Search Tool**: Warehouse inventory optimization system

---
-->
