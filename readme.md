# notes

## hugo version

hugo v0.122.0-b9a03bd59d5f71a529acb3e33f995e0ef332b3aa+extended darwin/arm64 BuildDate=2024-01-26T15:54:24Z VendorInfo=brew

# other

for the header.html

```html
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>{{ .Title }} | {{ .Site.Title }}</title>
  <link rel="stylesheet" href="{{ "css/style.css" | relURL }}" /> <link rel="stylesheet" href="{{ "css/fonts.css" |
  relURL }}" /> {{ partial "head_custom.html" . }}
</head>
```

had this in the contact me apart:

        {{ with .Site.Params.contact }}
        {{ . | markdownify }}

[params]
contact = '[<span><svg viewBox="0 0 15 15" fill="none" aria-hidden="true"><path d="M14.5.5l.46.197a.5.5 0 00-.657-.657L14.5.5zm-14 6l-.197-.46a.5.5 0 00-.06.889L.5 6.5zm8 8l-.429.257a.5.5 0 00.889-.06L8.5 14.5zM14.303.04l-14 6 .394.92 14-6-.394-.92zM.243 6.93l5 3 .514-.858-5-3-.514.858zM5.07 9.757l3 5 .858-.514-3-5-.858.514zm3.889 4.94l6-14-.92-.394-6 14 .92.394zM14.146.147l-9 9 .708.707 9-9-.708-.708z" fill="currentColor"></path></svg> contact me](mailto:graham.annett@gmail.com)
