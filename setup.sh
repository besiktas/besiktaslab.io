#!/bin/sh

brew install hugo

hugo server --watch --buildDrafts=true --ignoreCache --gc
